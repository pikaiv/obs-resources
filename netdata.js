window.netdata_metrics = function(){};

$(function () {
	setTimeout(update_metrics, 0);
});

function update_metrics () {

	var onSuccess = function ( data ) {
		// json = JSON.parse(data);
		// var array = data.entries;
		// console.log(data['system.cpu']['dimensions']['idle']['value']);
		$("#cpu_usage").html('CPU Usage: '+ Math.round(100 - data['system.cpu']['dimensions']['idle']['value'])+' %');
		$("#cpu_temp").html('CPU Temp: '+ data['sensors.k10temp-pci-00c3_temperature']['dimensions']['k10temp-pci-00c3_temp1']['value'] +' ℃');
		$("#gpu_usage").html('GPU Usage: '+ data['nvidia_smi.gpu0_gpu_utilization']['dimensions']['gpu0_gpu_util']['value']+' %');
		$("#gpu_temp").html('GPU Temp: '+ data['nvidia_smi.gpu0_temperature']['dimensions']['gpu0_gpu_temp']['value']+' ℃');
	}

	$.get('http://127.0.0.1:19999/api/v1/allmetrics?format=json', null, onSuccess);
	setTimeout(update_metrics, 1000);
}
