window.obs_bg_animation = function(){};
window.obs_bg_animation.canvas = null;
window.obs_bg_animation.context = null;
window.obs_bg_animation.wds;

class WaterDrop {
	constructor(size, position_x, position_y, direction_x, direction_y, alpha, variation_alpha) {
		this.size = size;
		this.position_x = position_x;
		this.position_y = position_y;
		this.direction_x = direction_x;
		this.direction_y = direction_y;
		this.alpha = alpha;
		this.variation_alpha = variation_alpha;
	}
}

$(function () {
	sizing();
	$(window).resize(function() {
		sizing();
	});
	window.obs_bg_animation.canvas = document.getElementById('area');
	if (window.obs_bg_animation.canvas.getContext) {
		window.obs_bg_animation.context = window.obs_bg_animation.canvas.getContext('2d');
		setTimeout(initialize_canvas, 0);
	}
});

function sizing () {
	$("#area").attr({height:$("#canvas_wrapper").height()});
	$("#area").attr({width:$("#canvas_wrapper").width()});
}

function initialize_canvas () {
	window.obs_bg_animation.wds = [];
	for ( i=0; i<100; i++ ) {
		window.obs_bg_animation.wds[i] = new WaterDrop(
			Math.floor(Math.random() * Math.floor(40)),
			Math.floor(Math.random() * Math.floor($("#canvas_wrapper").width())),
			Math.floor(Math.random() * Math.floor($("#canvas_wrapper").height())),
			(Math.random() * Math.floor(2) - 1) ,
			(Math.random() * Math.floor(2) - 1) ,
			Math.random(),
			(Math.random() * Math.floor(2) - 1 ) * 0.001
		);
	}
	setTimeout(render, 0);
}

function render () {
	window.obs_bg_animation.context.clearRect(0, 0, window.obs_bg_animation.canvas.width, window.obs_bg_animation.canvas.height);

	update_water_drops = function (index, val) {
		window.obs_bg_animation.context.beginPath();

		val.position_x += val.direction_x;
		if ( (val.position_x < 0) && (val.direction_x < 0) ) {
			val.direction_x *= -1;
		} else if ( (val.position_x > $("#canvas_wrapper").width()) && (val.direction_x > 0) ) {
			val.direction_x *= -1;
		}
		val.position_y += val.direction_y;
		if ( (val.position_y < 0) && (val.direction_y < 0) ) {
			val.direction_y *= -1;
		} else if ( (val.position_y > $("#canvas_wrapper").height()) && (val.direction_y > 0) ) {
			val.direction_y *= -1;
		}
		val.alpha += val.variation_alpha;
		if ( val.alpha > 0.6 ) {
			val.alpha = 0.6;
		} else if ( val.alpha < -0.05 ) {
			val.alpha = 0;
		}
		window.obs_bg_animation.context.arc(val.position_x, val.position_y, val.size, 0, (Math.PI * 2));
		window.obs_bg_animation.context.fillStyle = "rgba(245, 245, 255, "+ val.alpha +")";
		window.obs_bg_animation.context.fill();

		val.direction_x += ((Math.random() * 2) - 1) * 0.01;
		if ( val.direction_x > 2 ) {
			val.direction_x = 2;
		} else if ( val.direction_x < -2 ) {
			val.direction_x = -2;
		}
		val.direction_y += ((Math.random() * 2) - 1) * 0.01;
		if ( val.direction_y > 2 ) {
			val.direction_y = 2;
		} else if ( val.direction_y < -2 ) {
			val.direction_y = -2;
		}
		val.variation_alpha += ((Math.random() * 2) - 1) * 0.0001;
		if ( val.variation_alpha > 0.05 ) {
			val.variation_alpha = 0.05;
		} else if ( val.variation_alpha < -0.05 ) {
			val.variation_alpha = -0.05;
		}
	};

	$.each(window.obs_bg_animation.wds, update_water_drops);
	setTimeout(render, 250);
}
