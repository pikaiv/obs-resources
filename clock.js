window.obs_clock = function(){};

$("#header_login_state").html('<a href="javascript:open_login_window()">ログイン</a>');

$(function () {
    setTimeout(refresh_clock_time, 0);
});

function refresh_clock_time () {
    date = new Date();
    $("#clock").html(date.toLocaleTimeString([], {hour12: false}));
    setTimeout(refresh_clock_time, 1000 - date.getMilliseconds);
}
